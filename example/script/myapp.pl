#!/usr/bin/env perl

use strict;
use warnings;

use MyApp ();

my $data = MyApp::run(@ARGV);
while (my ($key, $value) = each %{$data}) {
  print "$key $value\n";
}

exit 0;
