package MyApp;

use strict;
use warnings;

use YAML::PP  ();

sub run {
  return YAML::PP::LoadFile(shift);
}

1;
