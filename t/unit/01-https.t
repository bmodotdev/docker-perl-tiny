use strict;
use warnings FATAL => 'all';

use HTTP::Tiny  ();
use Test::More;

my $url = 'https://github.com';
my $status = HTTP::Tiny->new->get($url)->{status};

is($status, 200, "GET $url");

done_testing();
