#!/usr/bin/env perl
package script::check_perl_versions;

use strict;
use warnings;

use HTTP::Tiny  ();
use JSON::PP    ();
use YAML::PP    ();

my $versions_url = 'https://fastapi.metacpan.org/v1/release/versions/perl';

exit(__PACKAGE__->new(@ARGV)->run) unless caller;

sub new {
  my $class = shift;
  my $versions_file = shift;

  die 'Must provide matrix file as argument'
    unless defined $versions_file;

  die sprintf('No such file: “%s”', $versions_file)
    unless -f $versions_file;

  die sprintf('Cannot read matrix file “%s”', $versions_file)
    unless -r $versions_file;

  return bless {
    versions_url  => $versions_url,
    versions_file => $versions_file,
  }, $class;
}

sub run {
  my $self = shift;

  my ($current) = $self->get_current_versions;
  my ($new) = $self->get_new_versions( keys %{ $current } );

  if (keys %{ $new }) {
    printf STDERR "New versions of Perl are available: %s\n",
      join(', ', keys %{ $new});
    return $self->exit_failure;
  }
  else {
    print "No new versions found\n";
    return $self->exit_success;
  }
}

sub get_new_versions {
  my $self = shift;
  my (@currents) = @_;
  my ($max_cur_major) = sort { $b <=> $a }
    map { (split(/\./,$_))[1] } @currents;

  my $response = HTTP::Tiny->new->get( $self->versions_url );

  unless ($response->{success}) {
    my $msg = sprintf("Failed to  fetch url “%s”: code: %s, reason: %s, content: %s\n",
      $self->versions_url, $response->{status}, $response->{reason}, $response->{content});
    die $msg;
  }

  my $data = JSON::PP->new->utf8->decode( $response->{content} );
  unless (defined $data->{releases}) {
    die "No releases were found: GET " . $self->versions_url;
  }

  # HoA: $major => [$minor, $data]
  my %versions;
  VERSION: for my $v (@{ $data->{releases} }) {

    # Skip developer versions
    next VERSION unless defined $v->{maturity} && lc($v->{maturity}) eq 'released';

    # Pull our major and minor verisions, skipping odd majors
    next VERSION unless defined $v->{name};
    next VERSION unless $v->{name} =~ m/^perl-5\.([0-9]*[02468])\.([0-9]+)$/;

    my ($new_major, $new_minor) = ($1, $2);
    my $keep_me = 0;

    CUR: for my $cur (@currents) {
      # perl-5.38.2
      my (undef, $cur_major, $cur_minor) =  split(/\./, $cur);

      # New major
      ++$keep_me and last CUR if $new_major > $max_cur_major;

      # New minor
      ++$keep_me if $new_major == $cur_major && $new_minor > $cur_minor;
    }

    next VERSION unless $keep_me;

    # Have we seen this verions already? If so, is it a new minor?
    next VERSION if exists $versions{ $new_major }
      && $versions{ $new_major }->[0] > $new_minor;

    $versions{ $new_major } = [$new_minor, $v];
  }

  return { map { $_->[1]->{name} => $_->[1] } values %versions };
}

sub get_current_versions {
  my $data = YAML::PP::LoadFile( $_[0]->versions_file );
  return { map { $_->{PERL_VERSION} => 1 } @{$data->{'.matrix'}} };
}

sub versions_url {
  return $_[0]->{versions_url};
}

sub versions_file {
  return $_[0]->{versions_file};
}

sub exit_failure { 1 };
sub exit_success { 0 };

1;
