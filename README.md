# Docker Perl Tiny
`docker-perl-tiny` provides a minimal Perl base image using [Chainguard](https://images.chainguard.dev)
images as the base, which are smaller in size and attack surface.
Chainguard claims to reduce CVE’s in images by 97.6% compared to official counterparts.

# Example Usage
See the [`example`](example/) Dockerfile:
```
FROM registry.gitlab.com/bmodotdev/docker-perl-tiny:5.36-devel AS devel
WORKDIR /nobody
COPY --chown=nobody:nobody cpanfile cpanfile.snapshot example.yml .
COPY --chown=nobody:nobody script/ script/
COPY --chown=nobody:nobody lib/ lib/
COPY --chown=nobody:nobody t/ t/
RUN carton install --deployment .
ENV PERL5LIB="/nobody/lib:/nobody/local/lib/perl5${PERL5LIB:+:$PERL5LIB}"
ENV PATH="/nobody/local/bin:$PATH"
CMD [ "/bin/sh" ]

FROM registry.gitlab.com/bmodotdev/docker-perl-tiny:5.36 AS tiny
WORKDIR /nobody
USER nobody
COPY --from=devel /nobody/local local/
COPY --chown=nobody:nobody example.yml .
COPY --chown=nobody:nobody script/ script/
COPY --chown=nobody:nobody lib/ lib/
ENV PERL5LIB="/nobody/lib:/nobody/local/lib/perl5${PERL5LIB:+:$PERL5LIB}"
ENTRYPOINT [ "perl", "/nobody/script/myapp.pl" ]
CMD [ "example.yml" ]
```

# [`matrix.yml`](matrix.yml)
This file is included via CI/CD to dynamically build various images.
This file is also read by [`check_perl_versions.pl`](check_perl_versions.pl), which queries
[metacpan.org](https://fastapi.metacpan.org/v1/release/versions/perl) and alerts if new versions of
Perl are found.
When newer versions of Perl are found, a maintainer should open a PR to update the matrix with
**verified** checksums.

# Image Tags & Variants
Images built against the default `main` branch exist in the “root” namespace of this image.
Images built against any other branch will exist in a subpath corresponding to the branch it was
built on.

The tags available for this image are:
* `5.38.2-devel` `5.38-devel` `5-devel` `devel`
* `5.38.2-tiny` `5.38-tiny` `5.38` `5` `tiny` `latest`
* `5.36.3-devel` `5.36-devel`
* `5.36.3-tiny` `5.36.3-tiny` `5.36`
* `5.34.3-devel` `5.34-devel`
* `5.34.3-tiny` `5.34-tiny` `5.34`

### `devel`
These images are based on Chainguard’s glibc image
[`cgr.dev/chainguard/wolfi-base`](https://images.chainguard.dev/directory/image/wolfi-base/overview).
`devel` images container a shell and package manager for extending the image.
The following Perl packages are included by default:
* Carton
* CPAN
* IO::Socket::SSL
* Perl::Critic
* Perl::Tidy
* Test::Harness

### `tiny`
These images are built from Chainguard’s `cgr.dev/chainguard/wolfi-base`; however,
only the essentials are copied into the final base image, Chainguard’s
[`cgr.dev/chainguard/static`](https://edu.chainguard.dev/chainguard/chainguard-images/reference/static/).
The following Perl packages are included by default:
* IO::Socket::SSL

