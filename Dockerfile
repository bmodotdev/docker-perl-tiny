FROM cgr.dev/chainguard/wolfi-base AS builder

ARG SRC_DIR="/usr/src/perl"
ARG SRC_TAR_URL="https://cpan.metacpan.org/authors/id/P/PE/PEVANS/perl-5.36.3.tar.gz"
ARG SRC_TAR_SUM="f2a1ad88116391a176262dd42dfc52ef22afb40f4c0e9810f15d561e6f1c726a"

WORKDIR "$SRC_DIR"

RUN apk update \
  && apk add --no-cache \
    build-base \
    openssl \
    openssl-dev \
    patch \
    wget \
    zlib-dev

RUN wget --no-verbose --show-progress --progress=dot:mega -O src.tar.gz "$SRC_TAR_URL" \
  && echo "${SRC_TAR_SUM} src.tar.gz" | sha256sum -c -w - \
  && tar --strip-components=1 -xaf src.tar.gz -C "$SRC_DIR" \
  && /bin/rm -v src.tar.gz \
  && cat *.patch |  patch -p1 \
  && archFlag=$(apk --print-arch | grep -q '64$' &&  echo '-Duse64bitall' || echo '-Duse64bitint') \
  && uname=$(uname -m) \
  && if [ "$uname" = x86_64 ]; then \ 
      gnuArch='x86_64-linux-gnu'; \
    elif [ "$uname" = aarch64 ] || [ "$uname" = arm64 ]; then \
      gnuArch='armhf-linux-gnueabi'; \
    elif [ "$uname" = ppc64 ]; then \
      gnuArch='powerpc-linux-gnu'; \
    fi \
  && ./Configure -Darchname="$gnuArch" "$archFlag" -Duseshrplib -Dvendorprefix=/usr/local  -des \
  && make -j$(nproc) \
  && TEST_JOBS=$(nproc) make test_harness \
  && make install \
  && /bin/rm -rf "$SRC_DIR"

WORKDIR /nobody
RUN cpan install IO::Socket::SSL
RUN apk del patch \
  wget

CMD [ "/bin/sh" ]

# BEGIN devel
FROM builder AS devel

WORKDIR /nobody

RUN cpan App::cpanminus \
  && cpanm Carton \
    Perl::Critic \
    Perl::Tidy \
    Test::Harness

# BEGIN tiny
FROM cgr.dev/chainguard/static AS tiny

COPY --chown=root:root --chmod=0644 etc_passwd /etc/passwd
USER nobody
WORKDIR /nobody

# OpenSSL
COPY --from=builder /usr/lib64/libssl.so.3 /usr/lib64/libssl.so.3
COPY --from=builder /usr/lib64/libcrypto.so.3 /usr/lib64/libssl.so.3

# ca-certificates-bundle
COPY --from=builder /etc/ssl /etc/ssl

# zlib-dev
COPY --from=builder /lib/libz.so.1 /lib/libz.so.1

# Perl
COPY --from=builder /usr/local/lib/perl5 /usr/local/lib/perl5
COPY --from=builder /lib/libm.so.6 /lib/libm.so.6
COPY --from=builder /lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2
COPY --from=builder /lib/libcrypt.so.1 /lib/libcrypt.so.1
COPY --from=builder /lib/libc.so.6 /lib/libc.so.6
COPY --from=builder /usr/local/bin/perl /usr/local/bin/perl

CMD [ "/usr/local/bin/perl", "-de0" ]
